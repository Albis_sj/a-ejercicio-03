import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {

  number1: number = 20;
  number2:number = 70;
  Resultado: number = this.number1 + this.number2;

  constructor() { }

  ngOnInit(): void {
  }

}
